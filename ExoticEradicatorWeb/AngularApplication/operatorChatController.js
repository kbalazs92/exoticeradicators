﻿ExoticEradicatorApp.controller('OperatorChatController', function ($scope, ExoticEradicatorService, AppState, ChatHubProxy) {

    var chatHub = ChatHubProxy('chatHub');

    chatHub.on('broadcastMessage', function (id) {
        if (id === AppState.getSelectedConversationId()) {
            $scope.loadSelectedConversation();
        }
    });

    ExoticEradicatorService.setCookieData('_' + Math.random().toString(36).substr(2, 9));

    $scope.loadSelectedConversation = function () {
        ExoticEradicatorService.loadConversation(AppState.getSelectedConversationId())
            .then(function (response) {
                console.log(response.data);
                $scope.messages = response.data;
            })
            .catch(function (error) {
                $scope.messages = [];
                console.log("An error happened while fetching the results.");
            });
    }

    $scope.sendMessageToCustomer = function postMessage() {
        console.log(AppState.getSelectedConversationId());
        ExoticEradicatorService.postMessageToCustomer(AppState.getSelectedConversationId(), $scope.newMessage)
            .then(function () {
                $scope.newMessage = "";
                chatHub.invoke('SendMessage', AppState.getSelectedConversationId());
            })
            .catch(function (error) {
                console.log("An error happened while sending the message.");
            });
    }

    $scope.$watch(function () { return AppState.getSelectedConversationId(); }, function (newValue, oldValue) {
        if (newValue !== oldValue) {
            $scope.loadSelectedConversation();
        }
    });
});