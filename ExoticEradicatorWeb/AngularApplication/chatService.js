﻿// TODO: This could be splitted further for operators and customers.
ExoticEradicatorApp.factory('ExoticEradicatorService', ['$http', '$cookies', function ($http, $cookies) {
    var ExoticEradicatorService = {};

    ExoticEradicatorService.setCookieData = function (username) {
        userName = username;
        $cookies.put("userId", username);
    };

    ExoticEradicatorService.getCookieData = function () {
        userName = $cookies.get("userId");
        return userName;
    };

    ExoticEradicatorService.clearCookieData = function () {
        userName = "";
        $cookies.remove("userId");
    };

    ExoticEradicatorService.getMessages = function () {
        return $http.get('/CustomerSupport/GetMessages');
    };

    ExoticEradicatorService.loadConversation = function (conversationId) {
        return $http({
            method: 'GET',
            url: '/HelpCenter/LoadConversation',
            params: { conversationId: conversationId }
        });
    };

    ExoticEradicatorService.getChatGroups = function () {
        return $http.get('/HelpCenter/GetChatGroups');
    };

    ExoticEradicatorService.postMessageToOperator = function (message) {
        return $http({
            method: 'POST',
            url: '/CustomerSupport/PostMessage',
            data: { "message": message }
        });
    };

    ExoticEradicatorService.postMessageToCustomer = function (selectedConversationId, message) {
        console.log(selectedConversationId);
        return $http({
            method: 'POST',
            url: '/HelpCenter/PostMessage',
            data: {
                "conversationId": selectedConversationId,
                "message": message
            }
        });
    };

    return ExoticEradicatorService;
}]);