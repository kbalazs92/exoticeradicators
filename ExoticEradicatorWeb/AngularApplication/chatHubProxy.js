﻿ExoticEradicatorApp.factory('ChatHubProxy', function ($rootScope) {
    function proxyFactory(hubName) {
        var connection = $.hubConnection();
        var proxy = connection.createHubProxy(hubName);

        connection.start().done(function () { });

        return {
            // Used for receiving messages from SignalR
            on: function (eventName, callback) {
                proxy.on(eventName, function (result) {
                    $rootScope.$apply(function () {
                        if (callback) {
                            callback(result);
                        }
                    });
                });
            },
            // Used for sending messages tos SignalR
            invoke: function (methodName, parameter, callback) {
                proxy.invoke(methodName, parameter)
                    .done(function (result) {
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback(result);
                            }
                        });
                    });
            }
        };
    };

    return proxyFactory;
});