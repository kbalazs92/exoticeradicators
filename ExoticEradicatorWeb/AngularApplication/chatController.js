﻿ExoticEradicatorApp.controller('ChatController', function ($scope, ExoticEradicatorService, AppState, ChatHubProxy) {

    var chatHub = ChatHubProxy('chatHub');

    // FIXME: Storing the conversationId in a cookie, and using SignalR's broadcast() method
    // is a serious security flaw!
    var conversationId = '_' + Math.random().toString(36).substr(2, 9);
    ExoticEradicatorService.clearCookieData(conversationId);
    ExoticEradicatorService.setCookieData(conversationId);

    chatHub.on('broadcastMessage', function (id) {
        if (id === ExoticEradicatorService.getCookieData()) {
            $scope.getMessages();
        }
    });

    $scope.getMessages = function () {
        ExoticEradicatorService.getMessages()
            .then(function (response) {
                $scope.messages = response.data;
            })
            .catch(function (error) {
                $scope.messages = [];
                console.log("An error happened while fetching the results.");
            });
    }

    $scope.sendMessage = function postMessage() {
        ExoticEradicatorService.postMessageToOperator($scope.newMessage)
            .then(function () {
                $scope.newMessage = "";
                chatHub.invoke('SendMessage', ExoticEradicatorService.getCookieData());
            })
            .catch(function (error) {
                console.log("An error happened while sending the message.");
            });
    }

    $scope.getMessages();
});