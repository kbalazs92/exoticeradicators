﻿ExoticEradicatorApp.factory('AppState', function () {
    var data = {
        SelectedConversationId: ''
    };

    return {
        getSelectedConversationId: function () {
            return data.SelectedConversationId;
        },
        setSelectedConversationId: function (selectedConversationId) {
            data.SelectedConversationId = selectedConversationId;
        }
    };
});