﻿ExoticEradicatorApp.controller('ChatGroupController', function ($scope, ExoticEradicatorService, AppState, ChatHubProxy) {

    var chatHub = ChatHubProxy('chatHub');

    chatHub.on('broadcastMessage', function (id) {
        var hasChatGroup = false;

        $scope.chatGroups.forEach(function (element) {
            if (element.ConversationId === id) {
                hasChatGroup = true;
            }
        });

        if (!hasChatGroup) {
            $scope.getChatGroups();
        }
    });

    $scope.getChatGroups = function () {
        ExoticEradicatorService.getChatGroups()
            .then(function (response) {
                $scope.chatGroups = response.data;
            })
            .catch(function (error) {
                $scope.messages = [];
                console.log("An error happened while fetching the results.");
            });
    }

    $scope.setSelectedConversationId = function (conversationId) {
        $scope.selectedConversationId = conversationId;
        AppState.setSelectedConversationId(conversationId);
    }

    $scope.getChatGroups();
});