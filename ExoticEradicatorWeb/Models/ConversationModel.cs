﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExoticEradicatorWeb.Models
{
    public class ConversationModel
    {
        public List<MessageModel> Messages { get; set; } = new List<MessageModel>();

        public string Username { get; set; }

        public string OperatorId { get; set; }

        public string ConversationId { get; set; }
    }
}