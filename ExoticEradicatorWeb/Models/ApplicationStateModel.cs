﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Web;

namespace ExoticEradicatorWeb.Models
{
    /// <summary>
    /// Singleton class to store the application's state (we don't want to persist anything).
    /// </summary>
    public sealed class ApplicationStateModel
    {
        private static readonly Lazy<ApplicationStateModel> lazy =
            new Lazy<ApplicationStateModel>(() => new ApplicationStateModel());

        public static ApplicationStateModel Instance { get { return lazy.Value; } }

        private ConcurrentDictionary<string, ConversationModel> conversations;

        private ApplicationStateModel()
        {
            this.conversations = new ConcurrentDictionary<string, ConversationModel>();
        }

        public List<ConversationModel> GetChatGroups(string userId)
        {
            return this.conversations.Values.Where(c => c.OperatorId == null || c.OperatorId == userId).ToList();
        }

        public List<MessageModel> GetMessages(string conversationId)
        {
            ConversationModel model;
            if (this.conversations.TryGetValue(conversationId, out model)) {
                return model.Messages;
            };

            return new List<MessageModel>();
        }

        public void PostCustomerMessage(string conversationId, string message)
        {
            this.StoreMessage(conversationId, message, true);
        }

        public void PostOperatorMessage(string conversationId, string message)
        {
            this.StoreMessage(conversationId, message, false);
        }

        private void StoreMessage(string conversationId, string message, bool isCustomerMessage)
        {
            var messageModel = new MessageModel
            {
                Date = DateTime.Now,
                Message = message,
                CustomerMessage = isCustomerMessage
            };

            var newModel = new ConversationModel()
            {
                Messages = new List<MessageModel> { messageModel },
                Username = Guid.NewGuid().ToString(),
                ConversationId = conversationId
            };

            this.conversations.AddOrUpdate(conversationId, newModel,
                (key, old) =>
                {
                    old.Messages.Add(messageModel);
                    return old;
                });
        }
    }
}