﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExoticEradicatorWeb.Models
{
    public class MessageModel
    {
        public bool CustomerMessage { get; set; }

        public DateTime Date { get; set; }

        public string Message { get; set; }
    }
}