﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace ExoticEradicatorWeb.SignalR
{
    /// <summary>
    /// Chat hub for signaling purposes (letting users know they received a new message)
    /// </summary>
    public class ChatHub : Hub
    {
        public void SendMessage(string conversationId)
        {
            // TODO: Instead of broadcasting, a single user should be targeted.
            Clients.All.broadcastMessage(conversationId);
        }
    }
}