﻿using ExoticEradicatorWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ExoticEradicatorWeb.Controllers
{
    [Authorize]
    public class HelpCenterController : Controller
    {
        private ApplicationStateModel state;

        public HelpCenterController()
        {
            state = ApplicationStateModel.Instance;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetChatGroups()
        {
            var cookie = HttpContext.Request.Cookies["userId"];
            return Json(this.state.GetChatGroups(cookie.Value), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult LoadConversation(string conversationId)
        {
            List<MessageModel> messages = this.state.GetMessages(conversationId);
            return Json(messages, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PostMessage(string conversationId, string message)
        {
            this.state.PostOperatorMessage(conversationId, message);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}