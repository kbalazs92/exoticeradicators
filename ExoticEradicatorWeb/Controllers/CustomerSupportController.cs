﻿using ExoticEradicatorWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace ExoticEradicatorWeb.Controllers
{
    public class CustomerSupportController : Controller
    {
        private ApplicationStateModel state;

        public CustomerSupportController()
        {
            state = ApplicationStateModel.Instance;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetMessages()
        {
            var cookie = HttpContext.Request.Cookies["userId"];
            List<MessageModel> messages = this.state.GetMessages(cookie.Value);
            return Json(messages, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PostMessage(string message)
        {
            var cookie = HttpContext.Request.Cookies["userId"];
            this.state.PostCustomerMessage(cookie.Value, message);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}