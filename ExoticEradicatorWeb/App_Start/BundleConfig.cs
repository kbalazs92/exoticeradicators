﻿using System.Web;
using System.Web.Optimization;

namespace ExoticEradicatorWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.3.1.js",
                        "~/Scripts/jquery.signalR-2.3.0.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/angular.js",
                "~/Scripts/angular-cookies.js"));

            bundles.Add(new ScriptBundle("~/bundles/webapp").Include(
                "~/AngularApplication/exoticEradicator.js",
                "~/AngularApplication/appState.js",
                "~/AngularApplication/chatController.js",
                "~/AngularApplication/operatorChatController.js",
                "~/AngularApplication/chatGroupController.js",
                "~/AngularApplication/chatHubProxy.js",
                "~/AngularApplication/chatService.js"));
        }
    }
}
